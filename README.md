# VotingAPI

A small API created to solve a coding challenge.


#Challenge

Business Goal:

Capture votes and display the results for a set of entities. The
entities could be videos (e.g.  youtube), pages or URLs (e.g.
facebook), candidates (democratic election), etc.

Your goal:

Build the backend REST API for the above business goal. Assume that
this API will be used by a different frontend team.


# Requirements

The operations/capabilities we expect to see would be:

* Create an entity
* Vote an entity up or down (up and down votes can cancel one another)
* List all of the entities, sorted by creation date
* View the total vote count for a given object

Other requirements
 * The API responses must be in JSON.
 * Some automated tests are required. What is tested and the type of tests are your decision.

 # Installation
 
 Installation is as simple as...
 
 * git cloning the repository: `git clone URL`
 * `npm install` within the same directory

