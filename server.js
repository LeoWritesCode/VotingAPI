const express   = require("express");
const parser    = require("body-parser");
const mongo     = require('mongodb').MongoClient;
const crypto    = require("crypto");

const mongoUrl  = "mongodb://localhost:8723";
//const dbName    = "votingAPI_session";
const port      = 8080;
const client    = new MongoClient(mongoUrl);
const app       = express();


async function newUser(crypto, username, password, mongoDBClient){

  const newUser = username;
  const secret = password;
  const hasher = crypto.createHmac("sha256", secret);
  const hash = hasher.update(username).digest('hex');
  const client = mongoDBClient;

  await client.connect();

  client.db("VotingAPI").collection('USERS').insertOne(
    { hash : newUser ,
      votedOn : { "CollectionName" : -1 }  
    }
  );
}//end newUser()

async function addCollection(mongoDBClient, CollectionName){
  const client = mongoDBClient;
  await client.connect();

  client.db("VotingAPI").collection(CollectionName)
};

async function addToCollection(mongoDBClient);

}//end newUser()


app.get("/", (req, res) => {
    res.send(`Listening on http://localhost:${port}`);
});


app.get("/session/([a-z][A-Z][0-9]){16}", (req, res) => {
  /* stores an object {"title" : [] } in mongo
   *
   *
  */
    res.send(`Listening on http://localhost:${port}`);
});

app.get("/votingAPI/voteUp", (req, res) => {
    res.send(`Listening on http://localhost:${port}`);
});

app.get("/votingAPI/voteDown", (req, res) => {
    res.send(`Listening on http://localhost:${port}`);
});


app.listen(port, () => {
    console.log(`app listening at http://localhost:${port}`)
});
